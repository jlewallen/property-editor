Ext.define('Manager.App', {
   extend : 'Ext.container.Viewport',
   requires : [ 'Manager.Tabs' ],

   initComponent : function() {
      Ext.define('Folder', {
         extend : 'Ext.data.Model',
         idProperty : 'path',
         fields : [ 'path', 'name' ],
         hasMany : [ {
            model : 'File',
            name : 'files'
         }, {
            model : 'Folder',
            name : 'folders'
         } ],
         proxy : {
            type : 'ajax',
            reader : {
               type : 'json',
               root : 'folder'
            },
            url : '/property-editor/folders.json'
         },
      });

      Ext.define('Property', {
         extend : 'Ext.data.Model',
         fields : [ 'key', 'value' ]
      });

      Ext.define('File', {
         extend : 'Ext.data.Model',
         idProperty : 'path',
         fields : [ 'path', 'name' ],
         hasMany : [ {
            model : 'Property',
            name : 'properties'
         } ],
         proxy : {
            type : 'ajax',
            reader : 'json'
         },
      });

      Ext.define('Node', {
         extend : 'Ext.data.Model',
         idProperty : 'path',
         fields : [ 'path', 'name', 'record' ],
         hasMany : 'children'
      });

      var self = this;
      Ext.apply(this, {
         layout : 'border',
         padding : 5,
         items : [ this.createTree(), this.createTabs() ]
      });
      this.callParent(arguments);
   },

   createTree : function() {
      this.store = Ext.create('Ext.data.TreeStore', {
         model : 'Node',
         proxy : {
            type : 'ajax',
            reader : {
               type : 'json',
               getData : function(data) {
                  function toFileNode(node) {
                     return {
                        name : node.name,
                        path : node.path,
                        type : 'properties',
                        url : "#" + node.path,
                        record : Ext.ModelMgr.create(node, "File"),
                        expanded : true,
                        children : []
                     };
                  }

                  function toTreeNode(node) {
                     var c = jQuery.merge(jQuery.map(node.folders, toTreeNode), jQuery.map(node.files, toFileNode));
                     return {
                        name : node.name,
                        path : node.path,
                        expanded : true,
                        children : c
                     };
                  }
                  return [ toTreeNode(data.folder) ];
               }
            },
            url : '/property-editor/folders.json'
         },
         folderSort : true
      });
      return this.tree = Ext.create('Ext.tree.Panel', {
         title : 'Folders',
         region : 'west',
         collapsible : true,
         width : 225,
         split : true,
         rootVisible : false,
         store : this.store,
         minWidth : 175,
         autoLoad : '/property-editor/folders.json',
         listeners : {
            scope : this,
            itemclick : function(source, node) {
               this.tabs.showFolder(node);
            }
         },
         columns : [ {
            xtype : 'treecolumn',
            text : 'Name',
            flex : 2,
            dataIndex : 'name'
         } ]
      });
   },

   createTabs : function() {
      return this.tabs = Ext.create('widget.tabs', {
         region : 'center',
         minWidth : 300,
         listeners : {
            scope : this
         }
      });
   }

});
