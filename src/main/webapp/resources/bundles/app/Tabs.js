Ext.define('Manager.Tabs', {
   extend : 'Ext.tab.Panel',
   alias : 'widget.tabs',
   maxTabWidth : 230,
   border : false,

   initComponent : function() {
      Ext.apply(this, {
         layout : 'border',
         tabBar : {
            border : true
         },
         padding : 5,
         items : []
      });
      this.opened = {};
      this.callParent();
   },

   showFolder : function(node) {
      if (!node) { return; }
      var record = node.get("record");
      if (!record) { return; }
      if (this.opened[node.getId()]) {
         this.setActiveTab(this.opened[node.getId()]);
         return;
      }
      var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
         clicksToEdit : 1
      });

      var store = Ext.create('Ext.data.Store', {
         autoDestroy : true,
         model : 'Property',
         data : record.get("properties"),
         proxy : {
            type : 'ajax',
            url : 'files/file.json',
            extraParams : {
               path : record.get("path")
            },
            reader : {
               type : 'json'
            },
            writer : {
               type : 'json',
               writeAll : true,
               writeRecords : function(req, data) {
                  var converted = {
                     name : '',
                     path : '',
                     properties : data
                  };
                  return Ext.data.writer.Json.prototype.writeRecords.apply(this, [ req, converted ]);
               }
            }
         }
      });
      var grid = Ext.create('Ext.grid.Panel', {
         plugins : [ cellEditing ],
         store : store,
         tbar : [ {
            text : 'Add',
            handler : function() {
               var r = Ext.ModelManager.create({
                  key : 'new.property',
                  value : 'value',
               }, 'Property');
               store.insert(0, r);
               cellEditing.startEditByPosition({
                  row : 0,
                  column : 0
               });
            }
         }, {
            text : 'Remove',
            handler : function() {
               var selected = grid.getView().getSelectionModel().getSelection();
               for ( var i = 0; i < selected.length; i++) {
                  store.remove(selected[i]);
               }
            }
         }, {
            text : 'Save',
            handler : function() {
               store.sync();
            }
         } ],
         columns : [ {
            text : 'Name',
            dataIndex : 'key',
            flex : 1,
            editor : {
               allowBlank : false
            }
         }, {
            text : 'Value',
            dataIndex : 'value',
            flex : 1,
            editor : {
               allowBlank : false
            }
         } ]
      });
      this.opened[node.getId()] = this.add(Ext.create('Ext.panel.Panel', {
         title : node.get("path"),
         closable : false,
         listeners : {
            scope : this
         },
         items : grid
      }));
   }
});
