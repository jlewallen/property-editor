package com.page5of4.pec.config;

import org.springframework.beans.factory.aspectj.AnnotationBeanConfigurerAspect;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = "com.page5of4.pec", excludeFilters = { @Filter(ExcludeFromScan.class), @Filter(type = FilterType.CUSTOM, value = ExcludeWebPackagesTypeFilter.class) }, scopedProxy = ScopedProxyMode.INTERFACES)
@EnableTransactionManagement(mode = AdviceMode.PROXY)
public class SpringConfig {
   @Bean
   public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
      PropertySourcesPlaceholderConfigurer bean = new PropertySourcesPlaceholderConfigurer();
      bean.setIgnoreUnresolvablePlaceholders(true);
      bean.setIgnoreResourceNotFound(true);
      return bean;
   }

   @Bean
   public Object configurableAspect() {
      return AnnotationBeanConfigurerAspect.aspectOf();
   }
}
