package com.page5of4.pec.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ExcludeFromScan
@Import(DefaultSpringMvcConfig.class)
@ComponentScan(basePackages = "com.page5of4.pec.web", excludeFilters = @Filter(ExcludeFromScan.class))
public class WebAppConfig {

}
