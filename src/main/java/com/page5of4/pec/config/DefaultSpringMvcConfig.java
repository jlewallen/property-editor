package com.page5of4.pec.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;

import com.page5of4.mustache.DefaultLayoutViewModelFactory;
import com.page5of4.mustache.I18nLambdaFactory;
import com.page5of4.mustache.MustacheViewEngine;
import com.page5of4.mustache.spring.ModelOnlyJacksonJsonView;
import com.page5of4.mustache.spring.MustacheView;
import com.page5of4.mustache.spring.MustacheViewResolver;
import com.page5of4.pec.web.ApplicationUrls;

@Configuration
@EnableWebMvc
@ExcludeFromScan
public class DefaultSpringMvcConfig extends WebMvcConfigurerAdapter {
   @Autowired
   private HttpServletRequest servletRequest;

   @Autowired
   private Environment env;

   @Autowired
   private ApplicationContext applicationContext;

   @Override
   public void addViewControllers(ViewControllerRegistry registry) {

   }

   @Override
   public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
      configurer.enable();
   }

   @Override
   public void addResourceHandlers(ResourceHandlerRegistry registry) {
      super.addResourceHandlers(registry);
      registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
   }

   @Bean
   public Object rootViewResolver() {
      ContentNegotiatingViewResolver bean = new ContentNegotiatingViewResolver();
      bean.setMediaTypes(getMediaTypes());
      bean.setViewResolvers(Arrays.asList(new BeanNameViewResolver(), viewResolver()));
      bean.setDefaultViews(Arrays.asList(modelOnlyJacksonView()));
      bean.setIgnoreAcceptHeader(true);
      bean.setDefaultContentType(MediaType.TEXT_HTML);
      bean.setParameterName("_format");
      bean.setFavorParameter(true);
      return bean;
   }

   @Bean
   public ApplicationUrls applicationUrls() {
      return new ApplicationUrls();
   }

   @Bean
   public View modelOnlyJacksonView() {
      ModelOnlyJacksonJsonView bean = new ModelOnlyJacksonJsonView();
      bean.setModelKey("model");
      return bean;
   }

   @Bean
   public ViewResolver beanNameViewResolver() {
      return new BeanNameViewResolver();
   }

   @Bean
   SimpleMappingExceptionResolver exceptionResolver() {
      SimpleMappingExceptionResolver bean = new SimpleMappingExceptionResolver();
      bean.setDefaultErrorView("exception");
      bean.setExceptionMappings(new Properties());
      return bean;
   }

   @Bean
   public ViewResolver viewResolver() {
      MustacheViewResolver bean = new MustacheViewResolver();
      bean.setViewClass(MustacheView.class);
      bean.setEngine(mustacheViewEngine());
      return bean;
   }

   @Bean
   public DefaultLayoutViewModelFactory defaultLayoutViewModelFatory() {
      return new DefaultLayoutViewModelFactory();
   }

   @Bean
   public MustacheViewEngine mustacheViewEngine() {
      return new MustacheViewEngine(defaultLayoutViewModelFatory(), servletRequest);
   }

   @Bean
   I18nLambdaFactory i18LambdaFactory() {
      return new I18nLambdaFactory();
   }

   public Map<String, String> getMediaTypes() {
      Map<String, String> mediaTypes = new HashMap<String, String>();
      mediaTypes.put("html", "text/html");
      mediaTypes.put("json", "application/json");
      return mediaTypes;
   }
}
