package com.page5of4.pec.domain;

public enum PauseType {
   ALL,
   CONSUMERS,
   PRODUCERS,
   NONE
}
