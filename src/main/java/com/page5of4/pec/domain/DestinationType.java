package com.page5of4.pec.domain;

public enum DestinationType {
   Queue,
   Topic
}
