package com.page5of4.pec.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.page5of4.mustache.SingleModelAndView;
import com.page5of4.pec.web.viewmodels.PropertyFileViewModel;
import com.page5of4.pec.web.viewmodels.factories.FolderViewModelFactory;

@Controller
@RequestMapping(value = "files")
public class FilesController {
   private final FolderViewModelFactory folderViewModelFactory;

   @Autowired
   public FilesController(FolderViewModelFactory folderViewModelFactory) {
      super();
      this.folderViewModelFactory = folderViewModelFactory;
   }

   @RequestMapping(value = "file", method = RequestMethod.GET)
   public ModelAndView show(@RequestParam String path) {
      return new SingleModelAndView("empty", folderViewModelFactory.createViewModel(path));
   }

   @RequestMapping(value = "file", method = RequestMethod.POST)
   public ModelAndView update(@RequestParam String path, @RequestBody PropertyFileViewModel file) {
      return new SingleModelAndView("empty", folderViewModelFactory.createViewModel(path));
   }
}
