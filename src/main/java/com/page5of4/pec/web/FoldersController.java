package com.page5of4.pec.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.page5of4.mustache.SingleModelAndView;
import com.page5of4.pec.web.viewmodels.factories.FolderViewModelFactory;

@Controller
@RequestMapping(value = "folders")
public class FoldersController {
   private final FolderViewModelFactory folderViewModelFactory;

   @Autowired
   public FoldersController(FolderViewModelFactory folderViewModelFactory) {
      super();
      this.folderViewModelFactory = folderViewModelFactory;
   }

   @RequestMapping(method = RequestMethod.GET)
   public ModelAndView folders(@RequestParam(defaultValue = "/") String path) {
      return new SingleModelAndView("folders/folders", folderViewModelFactory.createViewModel(path));
   }
}
