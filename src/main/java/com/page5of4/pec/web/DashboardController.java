package com.page5of4.pec.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.page5of4.mustache.SingleModelAndView;

@Controller
@RequestMapping(value = "/")
public class DashboardController {
   @RequestMapping
   public ModelAndView index() {
      return new SingleModelAndView("dashboard/dashboard", true);
   }

   public static class DashboardViewModel {}
}
