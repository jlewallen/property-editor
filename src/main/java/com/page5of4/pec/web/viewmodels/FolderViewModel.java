package com.page5of4.pec.web.viewmodels;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Lists;

public class FolderViewModel {
   private String name;
   private String path;
   private List<FolderViewModel> folders = Lists.newArrayList();
   private List<PropertyFileViewModel> files = Lists.newArrayList();

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getPath() {
      return path;
   }

   public void setPath(String path) {
      this.path = path;
   }

   public List<FolderViewModel> getFolders() {
      return folders;
   }

   public List<PropertyFileViewModel> getFiles() {
      return files;
   }

   public FolderViewModel() {
      super();
   }

   public FolderViewModel(String name, String path, List<FolderViewModel> folders, List<PropertyFileViewModel> files) {
      super();
      this.name = name;
      this.path = path;
      this.folders = folders;
      this.files = files;
   }

   public static FolderViewModel withFolders(String path, FolderViewModel... folders) {
      String name = new File(path).getName();
      return new FolderViewModel(name, path, Arrays.asList(folders), new ArrayList<PropertyFileViewModel>());
   }

   public static FolderViewModel withFiles(String path, PropertyFileViewModel... files) {
      String name = new File(path).getName();
      return new FolderViewModel(name, path, new ArrayList<FolderViewModel>(), Arrays.asList(files));
   }
}
