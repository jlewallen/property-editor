package com.page5of4.pec.web.viewmodels.factories;

import org.springframework.stereotype.Service;

import com.page5of4.pec.web.viewmodels.FolderViewModel;
import com.page5of4.pec.web.viewmodels.PropertyFileViewModel;

@Service
public class FolderViewModelFactory {
   public FolderRootViewModel createViewModel(String path) {
      FolderViewModel top = FolderViewModel.withFolders("configuration",
            FolderViewModel.withFolders("configuration/app1",
                  FolderViewModel.withFiles("configuration/app1/dev", PropertyFileViewModel.make("configuration/app1/dev/app.properties", "key1", "value1", "key2", "value2")),
                  FolderViewModel.withFiles("configuration/app1/production", PropertyFileViewModel.make("configuration/app1/production/app.properties", "key1", "value1", "key2", "value2"))
                  ),
            FolderViewModel.withFolders("configuration/app2",
                  FolderViewModel.withFiles("configuration/app2/dev", PropertyFileViewModel.make("configuration/app2/dev/app.properties", "key1", "value1", "key2", "value2")),
                  FolderViewModel.withFiles("configuration/app2/production", PropertyFileViewModel.make("configuration/app2/production/app.properties", "key1", "value1", "key2", "value2"))
                  ),
            FolderViewModel.withFolders("configuration/app3",
                  FolderViewModel.withFiles("configuration/app3/dev", PropertyFileViewModel.make("configuration/app3/dev/app.properties", "key1", "value1", "key2", "value2")),
                  FolderViewModel.withFiles("configuration/app3/production", PropertyFileViewModel.make("configuration/app3/production/app.properties", "key1", "value1", "key2", "value2"))
                  )
            );
      return new FolderRootViewModel(top);
   }

   public static class FolderRootViewModel {
      private final FolderViewModel folder;

      public FolderViewModel getFolder() {
         return folder;
      }

      public FolderRootViewModel(FolderViewModel folder) {
         super();
         this.folder = folder;
      }
   }
}
