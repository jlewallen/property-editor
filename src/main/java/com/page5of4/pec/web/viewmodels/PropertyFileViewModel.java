package com.page5of4.pec.web.viewmodels;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.google.common.collect.Lists;

public class PropertyFileViewModel {
   private String path;
   private String name;
   private List<PropertyViewModel> properties;

   public String getPath() {
      return path;
   }

   public void setPath(String path) {
      this.path = path;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public List<PropertyViewModel> getProperties() {
      return properties;
   }

   public void setProperties(List<PropertyViewModel> properties) {
      this.properties = properties;
   }

   public PropertyFileViewModel() {
      super();
   }

   public PropertyFileViewModel(String path, String name, List<PropertyViewModel> properties) {
      super();
      this.path = path;
      this.name = name;
      this.properties = properties;
   }

   public static PropertyFileViewModel make(String path, PropertyViewModel... properties) {
      String name = new File(path).getName();
      return new PropertyFileViewModel(path, name, Arrays.asList(properties));
   }

   public static PropertyFileViewModel make(String path, String... properties) {
      List<PropertyViewModel> viewModels = Lists.newArrayList();
      Iterator<String> iter = Arrays.asList(properties).iterator();
      while(iter.hasNext()) {
         viewModels.add(new PropertyViewModel(iter.next(), iter.next()));
      }
      return make(path, viewModels.toArray(new PropertyViewModel[0]));
   }
}
